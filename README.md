# binary_tree

La solucion de la prueba se desarrollo bajo Spring-Boot.

BRANCH test_solution


Las Heramientas necesarias para poder ejecutarlo son:

**Java 8**

**maven 3**



Para levantar el proyecto ejecutar siguiente comando desde la raiz:

mvn clean install spring-boot:run

![](https://i.imgur.com/bdjQBw8.png)

**EndPoints**

***Guardar Arbol***

[POST]

http://localhost:8080/binaryTree

{
	"tree": "67-76-85-87;67-76-85-83;67-76-74;67-39-44;67-39-28;67-39-28-29;"
}


***Encontrar ancestro comun mas bajo***

[GET]

http://localhost:8080/lowestCommonAncestor?id=1&node1=83&node2=87

LowestCommonAncestor(83 87) = 85